#!/bin/bash
set -eo pipefail
echo "Subiendo Mis imagenes a DockerHub"
filename="version"
while read -r line; do
    echo "$line" 
    docker tag angular-ui registry.gitlab.com/devemg/testing-gitlab-funcs/angular-ui:${line}
    docker push registry.gitlab.com/devemg/testing-gitlab-funcs/angular-ui:${line}
    echo "FIN DEL SCRIPT"
done < "$filename"

